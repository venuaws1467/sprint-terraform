output "ec2_public_ip" {
  value = ["${aws_instance.VM.*.public_ip}"]

  }

  output "ec2_private_ip" {
    value = ["${aws_instance.VM.*.private_ip}"]

    }

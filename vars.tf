variable "aws_profile" {
  default     = "raghu"
  description = "AWS profile name, as set in ~/.aws/credentials"
}

variable "aws_region" {
  type        = "string"
  default     = "ap-south-1"
  description = "AWS region in which to create resources"
}

variable "env_indicator" {
  type        = "string"
  default     = "dev"
  description = "What environment are we in?"
}

variable "icount" {

  default     = 1

}

variable "ami" {

 default ="ami-54d2a63b"

}

variable "bucket"{

default="terraform-002"
}


variable "type" {

default="t2.micro"

}

locals {

keyvalue= "profile"

}


variable "instance" {
  default = "something"
}

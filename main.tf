
provider "aws" {
  profile = "${var.aws_profile}"
  region  = "${var.aws_region}"
}

resource "aws_instance" "VM" {

  count = var.icount
  ami           = var.ami
  instance_type = var.type

  tags = {
      Environment = "${var.env_indicator}"
    }


}
